@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.post.post'))

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>@lang('strings.backend.post.posts')</strong>
                </div><!--card-header-->
                <div class="card-body">
                    <router-view name="postIndex"></router-view>
                    <router-view></router-view>
                </div><!--card-body-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
