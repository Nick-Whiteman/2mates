// Loaded after CoreUI app.js
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import PostIndex from './components/post/postIndex.vue';
import PostCreate from './components/post/postCreate.vue';
import PostEdit from './components/post/postEdit.vue';

const routes = [
    {
        path: '/',
        component: PostIndex,
        name: 'PostIndex'
    },
    {path: '/create', component: PostCreate, name: 'PostCreate'},
    {path: '/edit/:id', component: PostEdit, name: 'PostEdit'},
]

const router = new VueRouter({ routes })
const app = new Vue({ router }).$mount('#app')


