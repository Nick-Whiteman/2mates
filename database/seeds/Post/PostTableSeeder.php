<?php

use App\Models\Post;
use Illuminate\Database\Seeder;

/**
 * Class PostTableSeeder.
 */
class PostTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        Post::create([
            'title' => 'hanson robotics sophia',
            'body' => 'created by hanson robotics, ‘sophia’ the robot made her debut at the south by southwest show in march 2016 and since then has become somewhat of a media personality — having spoken at the united nations and appeared on the jimmy fallon show. she can animate a full range of facial expressions, and is able to track and recognize faces, look people in the eye, and hold natural conversations. in 2017, saudi arabia announced that it is giving citizenship to ‘sophia’, making it the first country in history to do so for a robot. on the achievement, ‘sophia’ had this to say, ‘I am very honored and proud for this unique distinction. this is historical to be the first robot in the world to be recognized with a citizenship.’',
        ]);

        Post::create([
            'title' => 'mayfield robotics kuri',
            'body' => 'mayfield robotics unveiled the intelligent ‘kuri’ robot at the CES 2017 trade show in las vegas. characterized by personality, awareness, and mobility, the robot is said to ‘add a spark of life to any home’. the smart bot can understand context and surroundings, recognize specific people, and respond to questions with facial expressions, head movements, and unique sounds. sourcing the many beloved robots of popular culture like R2-D2 and WALL-E, this mechanical buddy can also be defined by its adorable personality and extraordinary connectivity abilities.',
        ]);

        Post::create([
            'title' => 'sony aibo',
            'body' => 'sony announced the release of its latest robotic dog, ‘aibo’. this evolution of the autonomous robot can ‘form an emotional bond with members of the household while providing them with love, affection, and the joy of nurturing and raising a companion,’ sony says. featuring a dynamic range of movements and eager responsiveness, the puppy bot also develops its own unique personality as it grows closer to its owners. it uses ultracompact 1- and 2-axis actuators to give its compact body the freedom to move along a total of 22 axes.',
        ]);

        Post::create([
            'title' => 'stanford university snake robot',
            'body' => 'stanford university researchers have developed a snake-like robot that grows like a vine by squeezing through hard to reach places. the sole aim of the prototype is to act as a search and rescue device, moving through rubble and small openings in order to reach trapped survivors by delivering them water. the snake starts as a rolled up inside out tube with a pump on one end and a camera positioned on the other side. once initiated, the device inflates and grows in the direction of the camera, while the other side stays the same.',
        ]);

        Post::create([
            'title' => 'festo octopusgripper',
            'body' => 'just like an octopus has no hard skeleton and is made almost entirely of soft muscle, festo has applied this concept to soft robotics. the resulting creation is the ‘octupusgripper robotic’ arm—a flexible, silicone structure fitted with two rows of suction cups, just like its natural model. the ‘octopusgripper’ robot is the latest work of the company’s ‘bionic learning network,’ a series of robot’s that use biology as a model, copying the grip mechanisms of different animals. once compressed air has been applied and the tentacle curls inwards, it wraps around the object in question in a gentle—if a little creepy—manner, where a vacumm is used to its suction cups.',
        ]);
    }
}
